<?php

$utilisateur = $_POST[
    'utilisateur'];
$message = $_POST['message'];

if (empty($utilisateur) && empty($message)) {
    header('Location: viewTchat.php');
}
else {
    include 'connexion.php';

    $stmt = $connexion->prepare("INSERT INTO message VALUES (NULL , :nom, :message)");
    $stmt ->execute([
        'nom'=> $utilisateur,
        'message'=> $message
]);
    $ajouter=$stmt->fetch();
};
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="./Assets/style.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
<?php
$stmt = $connexion->query('SELECT * FROM message ORDER BY id DESC ');

include 'SendMessage.php';

echo '<table>';
while ($row= $stmt->fetch())
{
    echo '<tr>';
    echo '<td>';
    echo $row['id'];
    echo '</td>';
    echo '<td>';
    echo $row['nom'];
    echo '</td>';
    echo '<td>';
    echo $row['message'];
    echo '</td>';
    echo '</tr>';
}
echo '</table>';
?>
</body>
</html>